var gulp              = require('gulp'),
    sass              = require('gulp-sass'),
    browserSync       = require('browser-sync').create(),
    plumber           = require('gulp-plumber'),
    autoprefixer      = require('gulp-autoprefixer');


// Error Helper
var onError = function(err) { console.log(err); }

// Process Styles
gulp.task('styles', function() {
  return gulp.src('./src/style.scss')
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(gulp.dest('./dist'))
    .pipe(browserSync.stream());
});

gulp.task('serve', function() {
  browserSync.init({
    server: {
      baseDir: "./dist",
      index: "layout.html"
    }
  });
});

gulp.task('watch', function() {
  gulp.watch('./src/*.scss', ['styles']);
});

// Default Task
gulp.task('default', ['serve', 'watch']);
